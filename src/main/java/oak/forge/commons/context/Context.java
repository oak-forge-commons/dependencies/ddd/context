package oak.forge.commons.context;

import oak.forge.commons.aggregates.api.aggregate.AggregateRoot;
import oak.forge.commons.aggregates.api.command.exception.CommandRegisteringException;
import oak.forge.commons.aggregates.api.command.exception.NoSuchCommandMappingException;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.projections.NoSuchProjectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;

@SuppressWarnings("unchecked")
public class Context {

    private static final Logger LOG = LoggerFactory.getLogger(Context.class);

    private final Map<String, CommandEntry> commandMapping = new HashMap<>();
    private final Map<Class<?>, Function<Map<String, String>, Command>> commandFactories = new HashMap<>();
    private final Set<String> projections = new HashSet<>();

    public Context featureInfo(String message) {
        LOG.info("Registering [{}] feature", message);
        return this;
    }

    public <C extends Command, S extends AggregateState, A extends AggregateRoot<S>> Context registerCommandMapping
            (final String commandName, final Class<C> commandClass, final A aggregateRoot) {

        if (commandMapping.containsKey(commandName)) {
            throw new CommandRegisteringException(format("command with name [%s] has already been registered", commandName));
        }
        commandMapping.put(commandName, new CommandEntry<>(commandClass, aggregateRoot));
        LOG.info("command [{}] registered with class [{}] for aggregate root [{}]", commandName, commandClass.getName(), aggregateRoot);
        return this;
    }

    public <C extends Command, S extends AggregateState, A extends AggregateRoot<S>> Context registerCommandMapping
            (final String commandName,
             final Class<C> commandClass,
             final A aggregateRoot,
             final Function<Map<String, String>, Command> commandFactory) {
        registerCommandMapping(commandName, commandClass, aggregateRoot);
        commandFactories.put(commandClass, commandFactory);
        return this;
    }

    public Map<Class<?>, Function<Map<String, String>, Command>> getCommandFactories() {
        return commandFactories;
    }

    public <C extends Command> Class<C> getCommandClass(String commandName) {
        return ofNullable(commandMapping.get(commandName))
                .map(entry -> entry.commandClass)
                .orElseThrow(() -> new NoSuchCommandMappingException(commandName));
    }

    public <S extends AggregateState, A extends AggregateRoot<S>> A getAggregateRoot(String commandName) {
        return (A) ofNullable(commandMapping.get(commandName))
                .map(entry -> entry.aggregateRoot)
                .orElseThrow(() -> new NoSuchCommandMappingException(commandName));
    }

    private static class CommandEntry<C extends Command, S extends AggregateState, A extends AggregateRoot<S>> {
        final Class<C> commandClass;
        final A aggregateRoot;

        private CommandEntry(Class<C> commandClass, A aggregateRoot) {
            this.commandClass = commandClass;
            this.aggregateRoot = aggregateRoot;
        }


        @Override
        public String toString() {
            return format("%s -> %s", commandClass, aggregateRoot);
        }
    }

    public Context registerProjection(String projectionName) {
        projections.add(projectionName);
        LOG.info("projection registered: {}", projectionName);
        return this;
    }

    public void validateProjection(String projectionName) {
        if (!projections.contains(projectionName)) {
            throw new NoSuchProjectionException(format("no projection found for name [%s]", projectionName));
        }
    }

    @Override
    public String toString() {
        return "Context{" +
                "commandMapping=" + commandMapping +
                '}';
    }
}
